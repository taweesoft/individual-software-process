var Ship = cc.Sprite.extend({
	ctor: function(){
		this._super();
		this.initWithFile('res/ship.png');
	},
	update: function(dt){
    	var pos = this.getPosition();
    	if(pos.y>600)
            this.setPosition(new cc.Point(pos.x,0));
        else
    		this.setPosition(new cc.Point(pos.x,pos.y+5));
    	
    },
    onKeyDown: function( e ) {
    console.log( 'Down: ' + e );
    },
    onKeyUp: function( e ) {
    console.log( 'Up: ' + e );
    }
});